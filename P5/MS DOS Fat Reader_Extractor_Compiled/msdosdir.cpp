#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include "BootStrapSector.h"
#include "DirectoryEntry.h"

using namespace std;

// Produces a catalog listing of the given disk image.
int main(int argc, char *argv[])
{
  int imageHandle;
  
  BootStrapSector *boot;
  DirectoryEntry  **entries;
  
  byte *sn, *volumeLabel, *volumeLabelExt;
  
  int rootDirAddress, numEntries, e, numFiles  = 0, totalSize = 0;
  
  if (argc != 2){
    printf("Usage: %s FILE\n", argv[0]);
    printf("  FILE - The full or relative path of the disk image to read.\n");  
    return -1;
  }
  
  // Open a file handle to the disk image
  imageHandle = open(argv[1], O_RDONLY);
  
  if (imageHandle == -1)
  {
    printf("Failed to open file: %s\n", argv[1]); 
    return -1;
  }
  
  // Read the boot sector and locate the root directory
  boot = new BootStrapSector(imageHandle);
  
  sn = boot->get_VolumeSerialNumber();
  
  rootDirAddress = boot->get_BytesInReservedSectors() + (boot->get_BytesInFAT() * boot->get_CopiesFAT());
  
  numEntries = boot->get_EntriesInRootDir();
  
  // Scrape root directory entries
  entries = new DirectoryEntry*[boot->get_EntriesInRootDir()];
  
  for (e = 0; e < numEntries; e++) {
    entries[e] = new DirectoryEntry(imageHandle, rootDirAddress + (e * 32));
    if (entries[e]->is_VolumeLabel()) {

      // Volume label is actually found as a root directory entry, apparently...
      volumeLabel    = entries[e]->get_Name();
      volumeLabelExt = entries[e]->get_Extension();

    }
  }
  
  // Print catalog information
  printf("Volume name is %s%s\n", volumeLabel, volumeLabelExt);
  printf("Volume Serial Number is %X%X-%X%X\n", sn[3], sn[2], sn[1], sn[0]);
  printf("-------------------------------------------\n");
  
  for (e = 0; e < numEntries; e++){
    if (entries[e]->is_Deleted() || entries[e]->get_Name()[0] == 0x00 || entries[e]->is_VolumeLabel()) {
      // check for Deleted file, empty entry, or entry for volume label
    }
    else{
      // Actual file or subdirectory
      printf("%s %s  %7d  %02d-%02d-%d  %02d:%02d:%02d\n", entries[e]->get_Name(), entries[e]->get_Extension(), entries[e]->get_FileSize(),
        entries[e]->get_Month(), entries[e]->get_Day(), entries[e]->get_Year(), entries[e]->get_Hour(), entries[e]->get_Minute(),
        entries[e]->get_Second());
      
      numFiles++;
      totalSize += entries[e]->get_FileSize();
    }

  }
  
  printf("-------------------------------------------\n");
  printf(" %3d file(s)  %7d bytes\n", numFiles, totalSize);
  
  // Clean up allocated memory
  delete boot;
  boot = NULL;
  

  for (e = 0; e < numEntries; e++)  {
    delete entries[e];
    entries[e] = NULL;
  }
  
  delete [] entries;

  entries = NULL;
  
  close(imageHandle);
  
  return 0;
}

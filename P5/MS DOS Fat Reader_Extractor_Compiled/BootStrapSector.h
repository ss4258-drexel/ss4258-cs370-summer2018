#include <unistd.h>
#include <fcntl.h>

#define byte unsigned char

using namespace std;

class BootStrapSector
{
  public:
    BootStrapSector(int is);
    int   get_BytesInFAT();
    int   get_Clusters();
    int   get_EntriesInRootDir();
    int   get_BytesInReservedSectors();
    int   get_CopiesFAT();
    int   get_BytesPerCluster();
    byte* get_VolumeLabel();
    byte* get_VolumeSerialNumber();
    byte* get_FormatType();
  
  private:
    void readBootStrapSector();
    
    int imageStream;
    
    byte firstInstruction[3];  		
    byte sectorsPerCluster[1];
    byte OEM[8];  
    byte bytesPerSector[2];    
	byte copiesFAT[1];
    byte sectors[2];
    byte entriesRootDir[2];
    byte sectorsInFAT[2];
    byte sectorsPerTrack[2];
    byte mediaDescriptor[1];    
    byte sides[2];
    byte reservedSectors[2];    
    byte hiddenSectors[2];
	byte volumeSN[5]; 
	byte volumeLabel[12];   
	byte formatType[9]; 	// FAT12 or FAT16 
    byte hex55AA[2];		
   
    
};


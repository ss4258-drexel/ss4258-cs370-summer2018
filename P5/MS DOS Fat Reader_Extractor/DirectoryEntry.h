#include <fcntl.h>
#include <unistd.h>

#define byte unsigned char
#define DIRECTORY_ENTRY_H

class DirectoryEntry
{
  public:
   
    bool is_Deleted();

    bool is_ReadOnly();
    bool is_VolumeLabel();
    bool is_Subdirectory();
    bool is_Archived();
    bool is_Hidden();
    bool is_SystemFile();


    int get_Second();    
    int get_Minute();
    int get_Hour();
    int get_Day();
    int get_Month();
    int get_Year();

    
    int get_StartingCluster();
    int get_FileSize();

    DirectoryEntry(int is, int start);
    byte* get_Name();
    byte* get_Extension();
  
	private:
    void readDirectoryEntry();
	bool deleted;    
    int imageStream,startAddress;
    
    byte name[9];
    byte reserved[10];
    byte attributes[1];
    byte extension[4];
    byte time[2];
    byte fileSize[4];
    byte startingCluster[2];
    byte date[2];


};


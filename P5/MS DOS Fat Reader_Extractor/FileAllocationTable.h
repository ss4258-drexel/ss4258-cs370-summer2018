#include <fcntl.h>
#include <unistd.h>

#define byte unsigned char
using namespace std;

class FileAllocationTable{
 
	public:
	    FileAllocationTable(int is, int start);
    
    int get_Entry(int cluster);
  
	private:
    	int imageStream;
    	int startAddress;
};



#include "FileAllocationTable.h"

// Initializes a new FileAllocationTable
FileAllocationTable::FileAllocationTable(int is, int start){
  imageStream  = is;
  startAddress = start;
}

//Obtaining the FAT entry for the given cluster
int FileAllocationTable::get_Entry(int cluster){
	byte entry[2];
	int value;
  
	//Seek to the start of the FAT in the disk image
  	lseek(imageStream, startAddress, SEEK_SET);
  
  	lseek(imageStream, (3 * cluster) / 2, SEEK_CUR);
  	read(imageStream, entry, 2);
  
  	if (cluster%2 == 0)
    	value = ((entry[1] & 0x0F) << 8) + entry[0];
  	else
    	value = (entry[1] << 4) + ((entry[0] & 0xF0) >> 4);
  
  
return value;
}

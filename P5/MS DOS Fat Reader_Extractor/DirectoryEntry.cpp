#include "DirectoryEntry.h"

using namespace std;

// Initializes a directory entry. 

DirectoryEntry::DirectoryEntry(int is, int start){
  imageStream  = is;
  startAddress = start;
  
  readDirectoryEntry();
}

byte* DirectoryEntry::get_Name(){
  	return name;
}


byte* DirectoryEntry::get_Extension(){
  	return extension;
}

bool DirectoryEntry::is_Deleted(){
  	return deleted;
}

bool DirectoryEntry::is_ReadOnly(){
  	return (attributes[0] & 0x01) == 0x01;
}

bool DirectoryEntry::is_Hidden(){
  	return (attributes[0] & 0x02) == 0x02;
}

bool DirectoryEntry::is_SystemFile(){
  	return (attributes[0] & 0x04) == 0x04;
}

bool DirectoryEntry::is_VolumeLabel(){
  	return (attributes[0] & 0x08) == 0x08;
}

bool DirectoryEntry::is_Subdirectory(){
  	return (attributes[0] & 0x10) == 0x10;
}

bool DirectoryEntry::is_Archived()
{
  	return (attributes[0] & 0x20) == 0x20;
}


int DirectoryEntry::get_Second(){
  	return time[0] & 0x1F;
}

int DirectoryEntry::get_Minute(){
  	return ((time[1] & 0x07) << 3) + (time[0] >> 5);
}



int DirectoryEntry::get_Hour(){
  	return time[1] >> 3;
}

int DirectoryEntry::get_Day(){
  	return date[0] & 0x1F;
}


int DirectoryEntry::get_Month(){
  	return ((date[1] & 0x01) << 3) + (date[0] >> 5);
}

int DirectoryEntry::get_Year(){
  	return 1980 + (date[1] >> 1);
}

// Returns the starting cluster of the file or directory.  If the file is empty,
// this should be 0.
int DirectoryEntry::get_StartingCluster(){
  	return (startingCluster[1] << 8) + startingCluster[0];
}

// Returns the size of the file(bytes).
int DirectoryEntry::get_FileSize(){
	return (fileSize[3] << 24) + (fileSize[2] << 16) + (fileSize[1] << 8) + fileSize[0];
}

// Reads the directory entry from the disk image and populates the related fields.

void DirectoryEntry::readDirectoryEntry(){

  // Seek to the start of the entry in the disk image
  lseek(imageStream, startAddress, SEEK_SET);
  
  // Read values sequentially from the directory entry
  read(imageStream, name,            8);
  read(imageStream, extension,       3);
  read(imageStream, attributes,      1);
  read(imageStream, reserved,        10);
  read(imageStream, time,            2);
  read(imageStream, date,            2);
  read(imageStream, startingCluster, 2);
  read(imageStream, fileSize,        4);
  
  // Null-terminate string values
  name[8] = '\0';
  extension[3] = '\0';
  
  // Check for the deleted file
  deleted = (name[0] == 0xE5);
  
  // Fix file name if necessary
  if (name[0] == 0x05)
    name[0] = 0xE5;
  
}

#include "BootStrapSector.h"

// Initializes a new BootStrapSector.
BootStrapSector::BootStrapSector(int is){
	  imageStream = is;
	  readBootStrapSector();
}


// Returns the number of clusters on the disk.
int BootStrapSector::get_Clusters(){
  	int diskSectors = (sectors[1] << 8) + sectors[0];
  
  	return diskSectors / sectorsPerCluster[0];
}

// Returns the number of bytes in the FAT.
int BootStrapSector::get_BytesInFAT(){
  	int fatSectors  = (sectorsInFAT[1] << 8)   + sectorsInFAT[0],
      sectorBytes = (bytesPerSector[1] << 8) + bytesPerSector[0];
  
  	return fatSectors * sectorBytes;
}

// Returns the number of bytes in reserved sectors.
int BootStrapSector::get_BytesInReservedSectors(){
  	int reservedSec = (reservedSectors[1] << 8) + reservedSectors[0],
      sectorBytes = (bytesPerSector[1] << 8)  + bytesPerSector[0];
  
  	return reservedSec * sectorBytes;
}

// Returns the number of entries in the root directory.
int BootStrapSector::get_EntriesInRootDir(){
  	return (entriesRootDir[1] << 8) + entriesRootDir[0];
}



// Returns the total number of copies of the FAT.
int BootStrapSector::get_CopiesFAT(){
  	return copiesFAT[0];
}


// Returns the volume ID (serial number).
byte* BootStrapSector::get_VolumeSerialNumber(){
  	return volumeSN;
}

// Returns the FAT file system type, padded with blanks to a length of 8 bytes.
byte* BootStrapSector::get_FormatType(){
  	return formatType;
}

// Returns the volume label, padded with blanks to a length of 11 bytes.
byte* BootStrapSector::get_VolumeLabel(){
  	return volumeLabel;
}

// Returns the number of bytes per cluster.
int BootStrapSector::get_BytesPerCluster(){
  int sectorBytes = (bytesPerSector[1] << 8) + bytesPerSector[0];
  	return sectorsPerCluster[0] * sectorBytes;
}


void BootStrapSector::readBootStrapSector(){

	  // Read the values from the bootsector
	  read(imageStream, firstInstruction, 3);
	  read(imageStream, OEM, 8);
	  read(imageStream, bytesPerSector, 2);
	  read(imageStream, sectorsPerCluster, 1);
	  read(imageStream, reservedSectors, 2);
	  read(imageStream, copiesFAT, 1);
	  read(imageStream, entriesRootDir, 2);
	  read(imageStream, sectors, 2);
	  read(imageStream, mediaDescriptor, 1);
	  read(imageStream, sectorsInFAT, 2);
	  read(imageStream, sectorsPerTrack, 2);
	  read(imageStream, sides, 2);
	  read(imageStream, hiddenSectors, 2);
	  
	  // Read other values from the Extended BIOS Parameter Block
	  lseek(imageStream, 0x36, SEEK_SET);
	  read(imageStream, formatType, 8);
	  formatType[8] = '\0';
	  
	  lseek(imageStream, 0x2B, SEEK_SET);
	  read(imageStream, volumeLabel, 11);
	  volumeLabel[11] = '\0';
	  
	  lseek(imageStream, 0x27, SEEK_SET);
	  read(imageStream, volumeSN, 4);
	  volumeSN[4] = '\0';
	  
	  lseek(imageStream, 0x1FE, SEEK_SET);
	  read(imageStream, hex55AA, 2);
}

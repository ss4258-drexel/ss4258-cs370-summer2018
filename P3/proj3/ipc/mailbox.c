#include <linux/linkage.h>
#include <linux/kernel.h>
#include <linux/module.h>

void (*STUB_mysend)(pid_t pid, int n, char* buf) = NULL;
EXPORT_SYMBOL(STUB_mysend);

asmlinkage void sys_mysend(pid_t pid, int n, char *buf)
{
	if(STUB_mysend)
		return STUB_mysend(pid, n, buf);
	else
		return -ENOSYS;
}

int (*STUB_myreceive)(pid_t pid, int n, char* buf) = NULL;
EXPORT_SYMBOL(STUB_myreceive);

asmlinkage int sys_myreceive(pid_t pid, int n, char *buf)
{
	if(STUB_myreceive)
		return STUB_myreceive(pid, n, buf);
	else
		return -ENOSYS;
}


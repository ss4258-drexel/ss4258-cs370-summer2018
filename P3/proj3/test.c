#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/syscall.h>

/*                            *
 * 292 = send                 *
 * 293 = receive              */

#define SYS_SND 	292
#define SYS_RCV		293

#define CHILD_COUNT 	3
#define SIZE		CHILD_COUNT * sizeof(pid_t)

void message(pid_t child[3])
{
	int i;
	pid_t pid; 
	char buf[SIZE];
	pid_t children[CHILD_COUNT];

	pid = getpid();

	/* Wait for mail notification about other processes from parent. */
	while(1) {
		sleep(1);
		if (syscall(SYS_RCV, pid, SIZE, buf) != 0)
			break;
	}

	printf("Child process %d knows about processes: ", pid);
	for (i=0; i<CHILD_COUNT; i++) {
		children[i] = *((pid_t *)buf+i);
		printf("%d ", children[i]);
	}


	printf("\nSending out messages to them...");

	sleep(5);
	for (i=0; i<CHILD_COUNT; i++) {
		if (children[i] != pid)
			syscall(SYS_SND, children[i], 32, "Hello yo!");
	}

	i = CHILD_COUNT;
	char s[32];
	while (i != 0) {
		syscall(SYS_RCV, pid, 32, &s);	
		printf("%d: received a msg from brother!\n", pid);
		i--;
	}
} 

int main()
{
	int i;
	char s[SIZE];
	pid_t t;
	pid_t child[CHILD_COUNT];

	for (i=0; i<CHILD_COUNT; i++) {
		if ((child[i] = fork()) == 0) {
			message(child);
			return 1;
		}
		*((pid_t *)s+i) = child[i];
	}

	for (i=0; i<CHILD_COUNT; i++) {
		syscall(SYS_SND, child[i], SIZE, (char *)&child);
	}
	return 0;
}

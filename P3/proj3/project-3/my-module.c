#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/linkage.h>
#include <linux/unistd.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

MODULE_LICENSE("Dual BSD/GPL");

extern void (*STUB_mysend)(pid_t pid, int n, char __user *buf);
extern int (*STUB_myreceive)(pid_t pid, int n, char __user *buf);


LIST_HEAD(mailbox);
spinlock_t lock = SPIN_LOCK_UNLOCKED;

struct mail {
	struct list_head list;
	pid_t pid;
	void *buffer;
	int length;
};




asmlinkage void mysend(pid_t pid, int n, char __user *buf)
{
	void *kbuf;
	struct mail *m;

	printk(KERN_DEBUG "sys_mysend called.\n");
	if (pid < 1) 
		return;

	/* Copy the buffer in to kernel memory. */
	kbuf = kzalloc(n, GFP_USER | __GFP_REPEAT);
	if (copy_from_user(kbuf, buf, n)) {
		printk(KERN_DEBUG "Failed to allocate space for the new mail, exiting.\n");
		return;
	}
	
	spin_lock(&lock);
	list_for_each_entry(m, &mailbox, list) {
		if (m->pid == pid) {
			printk(KERN_DEBUG "Mail exists for pid, removing for new mail.\n");
			kfree(m->buffer);
			m->length = n;
			m->buffer = kbuf;
			spin_unlock(&lock);
			return;
		}
	}
	
	printk(KERN_DEBUG "Adding mail for pid %d.\n", pid);
	m = kmalloc(sizeof(struct mail), GFP_KERNEL);
	m->pid = pid;
	m->length = n;
	m->buffer = kbuf;
	list_add_tail(&m->list, &mailbox);
	spin_unlock(&lock);

}

int retrieve(struct mail *m, int n, char __user *buf)
{
	if (n > m->length)
		n = m->length;

	copy_to_user(buf, m->buffer, n); 
	list_del(&m->list);
	kfree(m->buffer);
	kfree(m);

	return n;
}

asmlinkage int myreceive(pid_t pid, int n, char __user *buf)
{
	int ret;
	struct mail *m;

	printk(KERN_DEBUG "sys_myreceive called.\n");

	/* If the pid is -1, return the first found mail found. */
	spin_lock(&lock);
	if (pid == -1 && !list_empty(&mailbox)) {
		m = list_first_entry(&mailbox, struct mail, list);
		ret = retrieve(m, n, buf);
		spin_unlock(&lock);
		return ret;
	}

	list_for_each_entry(m, &mailbox, list) {
		if (m->pid == pid) {
			printk(KERN_DEBUG "Found mail in mailbox, retrieving..\n");
			ret = retrieve(m, n, buf);
			spin_unlock(&lock);
			return ret;
		}
	}

	spin_unlock(&lock);
	return 0;
}


static int __init init_custom_syscalls(void)
{
	printk(KERN_DEBUG "Syscalls module loaded...\n");

	STUB_mysend = &mysend;
	STUB_myreceive = &myreceive;

	return 0;
}
static void __exit cleanup_custom_syscalls(void)
{
	STUB_mysend = NULL;
	STUB_myreceive = NULL;

	/* Clean up the mailbox. */

	printk(KERN_DEBUG "Syscalls module unloaded...\n");
	printk(KERN_DEBUG "DELETING\n");

	while (!list_empty(&mailbox)) {
		struct mail *m;
		m = list_first_entry(&mailbox, struct mail, list);
		list_del(&m->list);
		kfree(m->buffer);
		kfree(m);
	}
}


module_init(init_custom_syscalls);
module_exit(cleanup_custom_syscalls);

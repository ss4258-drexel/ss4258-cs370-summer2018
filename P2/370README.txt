Saugat Sthapit

CS 370
Project2 WIKI

Description
In this project, several system calls were added to Linux Kernel and tested.
Test programs for syscalls are located in ‘test’ folder in root directory. When tested, all syscalls work perfectly.

It was pretty challenging assignment since linux kernel was exposed. Reading lots of articles in the web really helps.

1. sys_steal()
Implemented in sched.c. This syscall takes a pid_t as an argument and sets the UID and EUID for the task to 0 giving it root privilege. It is tested using test_steal by passing PID of bash. After program is executed, run whoami on bash that shows we have root privilege.

2. sys_quad()
Implemented in sched.c
Syscall number: 287
This syscall takes a pid_t as an argument and increases the time slice of the task by a factor of 4 and returns the new timeslice on success and a -1 on failure. There were no issues while implementing.
It is tested using test_quad which returns the new quadrupled timeslice of target task

3. sys_swipe()
Implemented in sched.c
Syscall number: 288
This syscall takes a target pid_t and a victim pid_t as arguments and increases the target's timeslice by the sum of the timeslices of the victim and all it's children, and sets their timeslices to 0 effectively swiping their time slice. The method returns the total swiped timeslice. Having done sys_quad, swiping victim’s timeslice wasn’t a big deal. The syscall ensures that it is not swiping from the target task. task_struct was helpful as it help me understand to iterate children of victim.
It is tested using test_swipe to ensure that timeslice is swapped from victim and its children. The return value was greater than 0 which concludes that some time was taken away.

4. sys_zombify()
Implemented in sched.c
Syscall number: 290
This syscall takes a pid_t as argument, and sets the target task's state to EXIT_ZOMBIE. The syscall returns a 0 on success and a -1 on failure. It is iterated over the list of all processes looking for the target task and once found, it’s state was set to EXIT_ZOMBIE, and return. It was tough to implement this as I need to make sure the task killed isnt the interupt handler or init which confirms that process killed doesnt hold any locks
It is tested using test_zombify. PID of our bash was passed to the program. Loosing control of bash is expected.

5. sys_myjoin()
Implemented in sched.c
Syscall number: 291
This syscall takes in a target task PID, and blocks the current process until the target task has finished running. It requires some sharing between sys_myjoin() and do_exit(). I created a struct myjoin_shared in sched.h which has three fields: isWaiting — indicates whether process is to be woken up
*currentTask : pointer to the current task that has been put to sleep and needs to be woken up.
*targetTask : The task we are waiting for.

Details of implementation:
The shared object global’s instance was created to exit.c which makes it possible to share between do_exit() and sys_myjoin(). For actual implementation, please check exit.c
The way sys_join() works is by by iterating over all processes to find the target task. It uses double-checked locking to confirm that the target task found is not zombie. After this, it sets isWaiting flag in sharedObj to 1 and assigns sharedObj.currentTask and sharedObj.targetTask to their corresponding tasks. Afterwards, current task is set to TASK_UNINTERRUPTIBLE using set_current_state() which sets state of current task to the state passed in as an argument. With the help of scheduler, we are able to remove the current task from run queue and schedule another task in run queue. The best strategy to put current task to sleep and interrupt later is via the use of wake_up_processs() to wake up the sleeping task.Now, we can do_exit() from the target task.Checks for target task to confirm that the task that was exited was the target task were made too.If it is target task, then was_up_process() is called and the isWaitiing flag is set to 0. 
It is tested using test_myjoin and python program called ‘spin_process’recommended by one of the class-mate that runs empty while loop as long as specified in cmd argument.For now, it was ran to 60 seconds and pushed to background while running, took its PID to pass to test_myjoin. Bash shell gets locked until the spin_process gets completed.

6. sys_forcewrite()
Syscall number: 290
Implemented in read_write.c
This syscall is pretty similar to write() syscall except that it doesnt check file permissions and allows to write to all users regardless of their write permissions. Pretty Cool and Dangerous.Obviously, there are many ways to the approaches. When using write(), it needs to be opened using open() syscall which fails if attempted to open read-only file in O_WRONGLY or O_APPEND modes. My approach was that if I want to write read-only file, I open it using O_RONLY flag, but when you make call to forcewrite(), you will be allowed to write to it despite the  fact that the file is opened in read mode.
After checking sys_write(), it was clear that the permission check wasn’t implemented however it was calling to vfs_write(). So, forcewrite() is same as sys_write() except that it is calling my_vfs_write() instead of vfs_write(). This means I have to write my own vfs_write() or my_vfs_write() where the check permission part is omitted. Without this, user can now write to read-only file.
It is test using test_forcewrite. I created a file unwritable_file and made chmod as 500. Afterwards, it is passed as destination in test_forcewrite() and confirmed that it was able to write to the file.


::::::::::::::::::::::::::::Notes on Patch::::::::::

The patch file was generated by downloading a pristine copy of the kernel and then diff'ing using

diff -r /home/ss4258/pristine linux-2.6.22.19-cs543 > patch



Saugat Sthapit
P4

PROJECT 4 - SCHEDULING

Description
Here I modified the round robin scheduler to assign task time slices
equally among users, ignoring any priority or niceness values on the tasks.
Among each users processes the scheduler splits timeslices equally, given the
users total timeslice availability.

#### task_timeslice(struct task_struct *p)  ####	kernel/sched.c
I modified the task_timeslice function to assign its timeslice.
task_timeslice takes in a task which I grab the owner from, and get the
total number of processes that the owner possesses to determine the
timeslice we allocate.
The timeslice single unit is multiplied by our TIMESLICE_USER_FACTOR of 5 to
reduce the overhead of switching.

#### read_myproc(...) ####    fs/proc/proc_misc.c
I also added a proc entry to read out information for each scheduled task
along with its uid and timeslice value.  


#### spin.c   TEST ####
I created a program that will create a daemon that is assigned a userid and
spins to show the realtime cpu allocation in top.
# gcc -DUID=1001 spin.c -o spin1
# gcc -DUID=1002 spin.c -o spin2
# ./spin1
# ./spin2 && ./spin2 && ./spin2
# top


 top - 05:01:41 up 24 min,  1 user,  load average: 3.99, 3.89, 2.78            
 Tasks:  47 total,   5 running,  42 sleeping,   0 stopped,   0 zombie           
 Cpu(s): 65.4%us, 34.6%sy,  0.0%ni,  0.0%id,  0.0%wa,  0.0%hi,  0.0%si,  0.0%st
 Mem:    120420k total,    47224k used,    73196k free,     3392k buffers  
 Swap:   746980k total,        0k used,   746980k free,    25728k cached        
                                                                   
   PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND           
  1602 1001      25   0  3636  208  132 R 50.9  0.2   9:36.03 spin1
  1606 1002      25   0  3636  208  132 R 16.6  0.2   3:10.32 spin2
  1609 1002      25   0  3636  208  132 R 16.3  0.2   3:02.70 spin2
  1604 1002      25   0  3636  208  132 R 16.0  0.2   3:11.47 spin2
     1 root      15   0 10304  736  616 S  0.0  0.6   0:00.50 init
     2 root      11  -5     0    0    0 S  0.0  0.0   0:00.00 kthre
	........







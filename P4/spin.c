#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifndef UID
#error "Need to -DUID"
#endif

int main()
{
	signal(SIGCHLD, SIG_IGN);
	signal(SIGHUP, SIG_IGN);

	pid_t pid = fork();
	if(pid < 0)
		exit(EXIT_FAILURE);
	if(pid > 0)
		exit(EXIT_SUCCESS); 

	if(setuid(UID) < 0)
		exit(EXIT_FAILURE);
	if(setsid() < 0)
		exit(EXIT_FAILURE);
	
	umask(0);
	chdir("/");

	int x;
	for (x = sysconf(_SC_OPEN_MAX); x>=0; x++) {
		close(x);
	}
	
	while(1);
	
	return 0;
}

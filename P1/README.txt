Project 1: Booting Your Custom Kernel
Saugat Sthapit
Jul 12, 2018

In this project, we set up the environment, build kernel and modify to accept new command line argument “printme”. If this argument is present, the kernet prints “Hello World From Me!”; Main.c file is modified to add variable printme and added EXPORT_SYMBOL(printme). Afterwards, function set_printme is created. Its called via __setup(“printme”, set_printme).Finally in start_kernel, we checked if printme was checked and printed the message using printk.

Major difficulty was to get familiar with building kernel. I was confused by whether to modify my code on local,tux or the VM. Except this, I dont have any other difficulties.

To run, just use the modified ‘main.c’ and run the VM.

#include <stdio.h>
#include <stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include <sys/wait.h>

int child(void);
int grandChild(pid_t grandParentPid);

int main(void)
{
    /* forking to get a child process*/
    switch(fork())
    {
    /* error while forking*/
    case -1:
        break;
    /* fork successful*/
    case 0:
        child();
        break;

    default:
        break;
        ;

    }
    /* wait for child to finish*/
    wait(NULL);
	return EXIT_SUCCESS;
}

int child(void)
{
    /* retrieving parent Pid */
    pid_t parentPid = getppid();
    /*forking to get a grandchild process*/
    if (fork()==0)
    {
        grandChild(parentPid);
    }
    return 0;
}

int grandChild(pid_t grandParentPid)
{
    /* retrieving parent pid*/
    pid_t parentPid = getppid();
    /* retrieving our own pid*/
    pid_t ownPid = getpid();

    printf("My process id is %d, my parent's id is %d, and my GrandParent's id is %d\n",ownPid,parentPid,grandParentPid);
    return 0;
}
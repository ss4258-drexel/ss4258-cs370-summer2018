#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>

#define SIGCHILD1 SIGUSR1
#define SIGCHILD2 SIGUSR2
#define SIGCHILD3 SIGWINCH

int child1(void);
int child2(void);
int child3(void);

/* flag to check if childs terminated*/
int signalChild1;
int signalChild2;
int signalChild3;
static void signal_handler(int signal)
{
    switch( signal )
       {
        case SIGCHILD1:
            signalChild1 = 1;
            break;
        case SIGCHILD2:
            signalChild2 = 1;
            break;
        case SIGCHILD3:
            signalChild3 = 1;
            break;
        default:
               break;
       }
}

int main(void)
{
    pid_t child3Pid;
    signalChild1 = 0;
    signalChild2 = 0;
    signalChild3 = 0;

    /* register signal handler*/
    if( signal( SIGCHILD1, signal_handler) == SIG_ERR  )
    {
        printf("Unable to create handler for SIGCHILD1\n");
    }

    if( signal( SIGCHILD2, signal_handler) == SIG_ERR  )
    {
        printf("Unable to create handler for SIGCHILD2\n");
    }
    if( signal( SIGCHILD3, signal_handler) == SIG_ERR  )
    {
        printf("Unable to create handler for SIGCHILD3\n");
    }
    /* forking to get a child processes*/
    /* child1 */
    if (fork() ==0)
    {
        child1();
    }
    else
    {
        /* child 2 */
        if (fork() ==0)
        {
            child2();
        }
        else
        {
            /* child 3 */
            child3Pid = fork();
            if (child3Pid ==0)
            {
                child3();
            }
            else
           {

                /* wait for signal to be received*/
                while(!signalChild1 || !signalChild2)
                {
                    sleep(1);
                }
                printf("Child 1 & 2 terminated, sending signal to child 3 \n");
                kill(child3Pid,SIGCHILD3);

            }


        }
    }



    return EXIT_SUCCESS;
}

int child1(void)
{   /*send signal to parent*/
    kill( getppid(), SIGCHILD1 );
    printf("Child 1 Terminating\n");
    return 0;
}

int child2(void)
{

    sleep(10);
    /* send signal to parent*/
    kill( getppid(), SIGCHILD2 );
    printf("Child 2 Terminating\n");
    return 0;
}

int child3(void)
{

    while(!signalChild3)
    {
           usleep(1);
    }
    printf("Child 3 Terminating \n");
    sleep(2);
    return 0;
}

